# KatScript Jupyter Kernel

A kernel for running KatScript commands in Jupyter. Analyses are automatically
plotted if possible. One magic command is defined: `%reset`, to reinitialise
the model.
